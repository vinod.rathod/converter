package newpackage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.JSONException;

public class Currency {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter From Curreny: ");
		String curr1 = sc.next();
		System.out.println("Enter From Curreny: ");
		String curr2 = sc.next();
		System.out.println("Enter the amount");
		double amount = sc.nextDouble();
		String fromCurr =curr1.toUpperCase();
		String toCurr =curr2.toUpperCase();
		String url = "https://api.exchangeratesapi.io/latest?symbols="+ fromCurr +","+ toCurr +"&base=USD";
		getExchange(url, fromCurr, toCurr, amount);
	}
	
	public static void getExchange(String url, String fromCurr, String toCurr, double amount) {
		
		JSONObject jObj = null;
    	String json = "";
		
		try {
			URL urlForGetRequest = new URL(url);
		    String readLine = null;
		    HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
		    conection.setRequestMethod("GET");
		    int responseCode = conection.getResponseCode();
		    if (responseCode == HttpURLConnection.HTTP_OK) {
		        BufferedReader in = new BufferedReader(
		            new InputStreamReader(conection.getInputStream()));
		        StringBuffer response = new StringBuffer();
		        while ((readLine = in .readLine()) != null) {
		            response.append(readLine);
		        } in .close();
//		        System.out.println("JSON String Result " + response.toString());
		        
		        // Manipulating result
		        
		        try {
		        	InputStream is = new ByteArrayInputStream(response.toString().getBytes());
		        	
		            BufferedReader reader = new BufferedReader(new InputStreamReader(
		                    is, "iso-8859-1"), 8);
		            StringBuilder sb = new StringBuilder();
		            String line = null;
		            while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		            }
		            is.close();
		            json = sb.toString();

		        } catch (Exception e) {
		            System.out.println("Buffer Error, "+ "Error converting result " + e.toString());
		        }

		        // parse the string to a JSON object
		        try {
		            jObj = new JSONObject(json);
		            double curr1 = jObj.getJSONObject("rates").getDouble(fromCurr);
		            double curr2 = jObj.getJSONObject("rates").getDouble(toCurr);
		            double actualAmount = (amount / curr1) * curr2;
		            System.out.println(actualAmount);
		            
		        } catch (JSONException e) {
		            System.out.println("JSON Parser, " + "Error parsing data " + e.toString());
		            System.out.println("error on parse data in jsonparser.java");
		        }
		        
		        //end of manipulation
		        
		    } else {
		        System.out.println("GET NOT WORKED");
		    }
		}catch(Exception e) {
			System.out.println("Something went wrong");
		}    
	}

}
